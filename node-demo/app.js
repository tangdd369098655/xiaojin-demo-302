var express = require("express");
var app = express();
const cors = require("cors");
//跨域请求cors
app.use(
  cors({
    origin: "*",
    credentials: true,
  })
);
// code 200 请求
app.get("/success", function (req, res) {
  res.send("ok");
});
app.post("/success", function (req, res) {
  res.send("ok");
});
// code 500 请求
app.get("/error500", function (req, res) {
  res.sendStatus(500);
});
const urlInfo = {
  baidu: "https://www.baidu.com/",
  error: "http://localhost:3002/error500", // 这个接口会返回状态码500
  notFound: "http://localhost:3002/notfound", // 根本就没有这个接口
  success: "http://localhost:3002/success", // 200
};
app.get("/redirect-success", function (req, res) {
  res.redirect(302, urlInfo.success);
});
app.post("/redirect-success", function (req, res) {
  res.redirect(302, urlInfo.success);
});
app.get("/redirect-baidu", function (req, res) {
  res.redirect(302, urlInfo.baidu);
});
app.post("/redirect-baidu", function (req, res) {
  res.redirect(302, urlInfo.baidu);
});
app.get("/redirect-error", function (req, res) {
  res.redirect(302, urlInfo.error);
});
app.post("/redirect-error", function (req, res) {
  res.redirect(302, urlInfo.error);
});
app.get("/redirect-not-found", function (req, res) {
  res.redirect(302, urlInfo.notFound);
});
app.post("/redirect-not-found", function (req, res) {
  res.redirect(302, urlInfo.notFound);
});


var http = app.listen(3002, "127.0.0.1", function () {
  var httpInfo = http.address();
  console.log(`创建服务${httpInfo.address}:${httpInfo.port}成功`);
});
